FYI EX1-2-SomewhatAdvancedThingy on Windows 32 bit. 
Ex 3-4 (although they are more than 2 exercises) is done on linux. 

Added some extra exercises. 

Folder Exercise3-4 contains CTF themed exercises from rop-emporium. I manually exploited the exercises to figure out what the payload would look like. The conversion process after the payload structure is determined is quite simple really. 

 That idea to take a single line of assembly code and find the corresponding binary instruction for it won't work because sometimes an instruction won't have a corresponding gadget. One could potentially combine many gadgets together to functionally be the same as that one gadget. Needs more thought put into it. 

type exercises in gadget chaining, which includes the combination of creating chains to do specialised tasks like write to memory. 

Folder SomewhatAdvancedThingy contains a tutorial to crack an actual real-world program. It also shows some ways to bypass existing anti-ROP techniques. It is from fuzzysecurity.com. 

ToDO:ADD vmi-based c files for each of the exercises. 

Actually developing a good intuition at for picking out gadgets and creating ROP chains is the most difficult task of all, so practice is necessary.  

The task of actually exploiting is quite simple after developing creating exploits using the hard-to-hone intuition. 
