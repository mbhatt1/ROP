Contains a bunch of small CTF-type challenges. Everytime, the objective becomes to open a file flag.txt via exploiting the binary in each folder. 

Source code was always provided with the binary until this point. Now, the training wheels are off!

To complete exercises in this module, you need to have radare2 installed. If you want to see the exploits working inside the VMs, execute the python scripts in the VM. To exploit them from outside the VM using memory instrumentation, libvmi, compile the exploit.c files provided to you. 

Maybe might need Binary Ninja. Also, if you already are a veteran sifu, go ahead and do as you please. 

Each folder contains an replace_return.c file(TODO). To compile each of them, do the following on your command line:

gcc -o exploit replace_return.c -lvmi

The program should display the stack contents after each 4 bytes get written such that you can see the changes. 

Also this is how I normally talk in my comments. I was trying to be formal earlier. 

Also, these are created for a linux based machine. Using Ubuntu 16.04 LTS did the trick for me. 



